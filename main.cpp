#include <fstream>
#include <dpp/dpp.h>
#include <spdlog/spdlog.h>
#include <dpp/nlohmann/json.hpp>

json load(const std::string &filename)
{
    json j;
    std::ifstream i(filename);
    i >> j;
    return j;
}

struct GuildConfig
{
    std::unordered_map<dpp::snowflake, json> messages;

    explicit GuildConfig(const json &j)
    {
        for (auto &[key, value]: j.items())
        {
            auto message_id = std::stoull(key);
            messages.insert_or_assign(message_id, value);
        }
    }

    std::optional<dpp::snowflake> role_for(dpp::snowflake message_id, const std::string &emoji)
    {
        if (messages.contains(message_id))
        {
            if (messages.at(message_id).contains(emoji))
            {
                return messages.at(message_id)[emoji].get<dpp::snowflake>();
            }
        }
        return {};
    }
};

struct Config
{
    std::string token;
    std::unordered_map<dpp::snowflake, GuildConfig> guilds;

    explicit Config(json j)
    {
        token = j["token"].get<std::string>();
        for (auto &[id, guild]: j["guilds"].items())
        {
            dpp::snowflake _id = std::stoull(id, nullptr, 0);
            guilds.insert_or_assign(_id, GuildConfig(guild));
        }
    }

    explicit Config(const char *filename) : Config(load(filename))
    {}

    std::optional<GuildConfig> guild(dpp::snowflake id)
    {
        if (guilds.contains(id))
        {
            return guilds.at(id);
        }
        return {};
    }
};

int main(int argc, const char *argv[])
{
    if (argc <= 1)
    {
        fmt::print("usage: {} <config file>", argv[0]);
        exit(1);
    }
    Config config(argv[1]);

    dpp::cluster bot(config.token);

    bot.on_message_reaction_add(
            [&bot, &config](const dpp::message_reaction_add_t &event)
            {
                auto _guild_config = config.guild(event.reacting_guild->id);
                if (_guild_config.has_value())
                {
                    auto guild_config = _guild_config.value();
                    auto emoji = event.reacting_emoji;
                    auto role = guild_config.role_for(
                            event.message_id,
                            emoji.name
                    );
                    if (role.has_value())
                    {
                        bot.guild_member_add_role(
                                event.reacting_guild->id,
                                event.reacting_user.id,
                                role.value()
                        );
                    }
                }
                else
                {
                    spdlog::error("no config for guild \"{}\"", event.reacting_guild->name);
                }
            }
    );

    bot.on_message_reaction_remove(
            [&bot, &config](const dpp::message_reaction_remove_t &event)
            {
                auto _guild_config = config.guild(event.reacting_guild->id);
                if (_guild_config.has_value())
                {
                    auto guild_config = _guild_config.value();
                    auto emoji = event.reacting_emoji;
                    auto role = guild_config.role_for(
                            event.message_id,
                            emoji.name
                    );
                    if (role.has_value())
                    {
                        bot.guild_member_remove_role(
                                event.reacting_guild->id,
                                event.reacting_user_id,
                                role.value()
                        );
                    }
                }
                else
                {
                    spdlog::error("no config for guild \"{}\"", event.reacting_guild->name);
                }
            }
    );

    bot.start(false);
}
